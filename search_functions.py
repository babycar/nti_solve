# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 18:35:49 2019

@author: Zoya
"""

import numpy as np
import cv2

def dist(coor1, coor2):
    return ((coor1[0] - coor2[0])**2 
            + (coor1[1] - coor2[1])**2)**0.5
           
def find_field_edges(frame, aruko_params):
    center = (frame.shape[1]//2, frame.shape[0]//2)
    corners, ids, rejectedImgPoints = aruko_params
    
    ids = [id_[0] for id_ in ids]
    ids_coros = dict(zip(ids, corners))
    warped_size = [[0,0], [0, 10*30-1], [30*8 - 1, 10*30-1], [30*8-1, 0]]
    marker_id_order = [5,6,8,7]
    
    coords_towarp = []
    field_edges = []
    frame = {}    
    bag = 0
    for k, i in enumerate(marker_id_order):
        try:
            smcorner = sorted(ids_coros[i][0], key = lambda coor: dist(coor, center))
            frame[i] = (smcorner[0][0],smcorner[0][1])
            coords_towarp.append(warped_size[k])
            field_edges.append(list(frame[i]))
        except:
            bag = 1
            continue
    return (len(list(frame.keys())), field_edges, coords_towarp)
    
def warp_by_4markers(frame, aruko_params):
    angle_count, field_edges, coords_towarp = find_field_edges(frame, aruko_params)
    rows, cols = (10*30-1, 30*8-1)
    
    if angle_count == 4:
        coords_towarp = np.float32([coords_towarp[:]])
        field_edges = np.float32([field_edges[:]])
        M = cv2.getPerspectiveTransform(field_edges, coords_towarp)
        warped_img = cv2.warpPerspective(frame,M,(cols,rows))
        return warped_img
        
    return warped_img

def warp_by_2markers(frame, aruko_params):
    pass
    
def color_coords(img):    
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)    
    
    lower_blue = np.array([0,0,200])
    upper_blue = np.array([180,255,255])
    
    mask = cv2.inRange(hsv, lower_blue, upper_blue)
    kernel = np.ones((11,11),np.uint8)
    mask = cv2.erode(mask,kernel,iterations = 1)
    res = cv2.bitwise_and(img, img, mask= mask)
    res = cv2.GaussianBlur(res,(7,7),0)
    
    gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(gray,127,255,0)
    im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    centers = []
    clr = ["r","y","g"]
    for cnt in contours:
        #желтый , зеленый, красный
        M = cv2.moments(cnt)
        cX = int(M["m10"] / M["m00"])
        cY= int(M["m01"] / M["m00"])
        centers.append((cX,cY, hsv[cY, cX][0]))

    centers = sorted(centers, key = lambda lst: lst[2])
    colors = [(center[0],center[1], color) for center, color in zip(centers, clr)]    
    return colors

def change_coord_sys(obj, cell_shape):
    x = obj[0]//cell_shape[0]
    y = obj[1]//cell_shape[1]
    return (x,y)
    
def get_objects(img):
    field_width, field_height = img.shape[:2]
    cell_shape = field_width/10,  field_height/8
    
    colors = color_coords(img)
    objects = []
    for color in colors:
        img_coor = color[:-1]
        field_coor = change_coord_sys(img_coor, cell_shape)
        objects.append([color[-1], img_coor, field_coor])
        
    return objects
#def  get_warped_field(frame):
#    pass