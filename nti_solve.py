# -*- coding: utf-8 -*-
"""
Created on Sun Dec  2 21:23:34 2018

@author: Zoya
"""

import cv2
from cv2 import aruco
import matplotlib.pyplot as plt
from search_functions import warp_by_4markers, get_objects

    
def detect(filename):  
    
    frame =cv2.imread(filename)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters =  aruco.DetectorParameters_create()               
    aruko_paraps = aruco.detectMarkers(gray, aruco_dict, 
                                                          parameters=parameters)  
    
#    while(True):     
    frame =cv2.imread(filename)
    warped_img = warp_by_4markers(frame, aruko_paraps)
    objects = get_objects(warped_img)
    
    for obj in objects:
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(warped_img, obj[0], obj[1], font, 1,(255,255,255),2,cv2.LINE_AA)
    plt.subplot(121),plt.imshow(warped_img),plt.title('Input')
       
    return objects

if __name__ == "__main__":
    i = 1037
    solve = detect("other\\cadr{0}.png".format(i))
    print(solve)
    r = 0
