
## Общий алгоритм решения задачи

1) На фотографии находятся ARuko маркеры.

2) По внутренним углам маркеров находится сетка.

3) В зависимости от положения маркеров сетка разворачивается таким образом, чтобы начало координат было в нижнем левом углу.

4) Получаются все цветные клетки сетки и определятся их цвета. 

###  Рассмотрим по отдельности каждый пункт.

#### Общий алгоритм

Для того, чтобы не писать самим алгоритмы распознования нужно установить библиотеку OpenCV и отдельный модуль из нее для распознавания маркеров aruko.

Далее подключаем эти библиотеки.


```python
import cv2
from cv2 import aruco
```

Главная функция программы


```python
def detect(filename):  
    # код, который находит маркеры
    frame =cv2.imread(filename)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters =  aruco.DetectorParameters_create()               
    aruko_paraps = aruco.detectMarkers(gray, aruco_dict, 
                                                          parameters=parameters)  
    
    
    frame =cv2.imread(filename)
    #получение сетки между маркерами
    warped_img = warp_by_4markers(frame, aruko_paraps)
    
    #получение объектов (цветных клеток) на сетке
    objects = get_objects(warped_img)
    
    #подписываем объекты на сетке 
    for obj in objects:
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(warped_img, obj[0], obj[1], font, 1,(255,255,255),2,cv2.LINE_AA)
    plt.subplot(121),plt.imshow(warped_img),plt.title('Input')
       
    return objects
```

### Получение сетки между маркерами

#### Алгоритм работы данного блока:

1) Находим внутренние углы маркеров. 
Для этого нужно найти у каждого маркера те углы, которые ближе всего к центру. Далее сортируем их по правильному порядку следования Aruko маркеров, как на картинке.

2) С помощью афинных преобразований преобразуем часть изображения между внутренними углами к прямому. 


![](img/normal.jpg)


<b>Получение внутренних углов


```python
def find_field_edges(frame, aruko_params):
    center = (frame.shape[1]//2, frame.shape[0]//2)
    corners, ids, rejectedImgPoints = aruko_params
    
    ids = [id_[0] for id_ in ids]
    ids_coros = dict(zip(ids, corners))
    #углы к которым мы преобразуем изображение
    warped_size = [[0,0], [0, 10*30-1], [30*8 - 1, 10*30-1], [30*8-1, 0]]
    # очередь Aruko маркеров, для того чтобы в дальнейшем правильно развернуть сетку.
    marker_id_order = [5,6,8,7]
    
    coords_towarp = []
    field_edges = []
    frame = {}    
    bag = 0
    for k, i in enumerate(marker_id_order):
        try:
            smcorner = sorted(ids_coros[i][0], key = lambda coor: dist(coor, center))
            frame[i] = (smcorner[0][0],smcorner[0][1])
            coords_towarp.append(warped_size[k])
            field_edges.append(list(frame[i]))
        except:
            bag = 1
            continue
    return (len(list(frame.keys())), field_edges, coords_towarp)

```

<b> Код для афинных преобразований


```python
def warp_by_4markers(frame, aruko_params):
    angle_count, field_edges, coords_towarp = find_field_edges(frame, aruko_params)
    rows, cols = (10*30-1, 30*8-1)
    
    if angle_count == 4:
        coords_towarp = np.float32([coords_towarp[:]])
        field_edges = np.float32([field_edges[:]])
        M = cv2.getPerspectiveTransform(field_edges, coords_towarp)
        warped_img = cv2.warpPerspective(frame,M,(cols,rows))
        return warped_img
        
    return warped_img
```

#### Результат работы кода:

Рисунок1 - оригинальное фото

Рисунок2 - вырезанная и преобразованная сетка


![Картинка][image1] ![Картинка][image2]


[image1]: img/cadr1037.png
[image2]: img/res1037.png




### Получение объектов (цвеных клеток) на сетке

Для начала нам нужно просто определить нахождение всех цветных квадратиков на сектке. Для этого можно бинаризировать изображение по яркости (объекты на сетке светлее, чем окружающие их клетки). Чтобы определить степень яркости переведем изображение из модели RGB в модель HSV. 
Чтобы определить какого цвета каждый объект, отсортируем их по параметру H(цветовому тону). Соответсвенно, вначале списка тогда окажется красный, потом желтый затем зеленый.

#### Алгоритм работы данного блока

1) Переводим изображение в HSV

2) Бинаризируем изображения по яркости и насыщенности. Применяем размытие гаусса, чтобы убрать лишние проблески.

3) Для того, чтобы отделить объекты между собой, находим на изображении контуры. 

4) Находим центральный пиксель каждого контура, определяем его цвет в модели HSV. Формируем список по типу:

[(значение пикселя в HSV, X, Y) , (значение пикселя в HSV, X, Y)]

5) Сортируем по параметру H.

6) Переводим координаты объектов из координат на изображении к координатам на сетке.



#### Получение координат объектов на изображении


```python
def color_coords(img):    
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)    
    
    #Бинаризаця
    lower_blue = np.array([0,0,200])
    upper_blue = np.array([180,255,255])
    
    mask = cv2.inRange(hsv, lower_blue, upper_blue)
    kernel = np.ones((11,11),np.uint8)
    mask = cv2.erode(mask,kernel,iterations = 1)
    res = cv2.bitwise_and(img, img, mask= mask)
    res = cv2.GaussianBlur(res,(7,7),0)
    
    gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(gray,127,255,0)
    #получение контуров
    im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    centers = []
    clr = ["r","y","g"]
    for cnt in contours:
        #желтый , зеленый, красный
        M = cv2.moments(cnt)
        cX = int(M["m10"] / M["m00"])
        cY= int(M["m01"] / M["m00"])
        #формирование списка центров
        centers.append((cX,cY, hsv[cY, cX][0]))
        
    #Сортировка по значению Hue
    centers = sorted(centers, key = lambda lst: lst[2])
    colors = [(center[0],center[1], color) for center, color in zip(centers, clr)]    
    return colors

```

#### Преобразование к координатам на сетке


```python
def get_objects(img):
    field_width, field_height = img.shape[:2]
    cell_shape = field_width/10,  field_height/8
    
    colors = color_coords(img)
    objects = []
    for color in colors:
        img_coor = color[:-1]
        field_coor = change_coord_sys(img_coor, cell_shape)
        objects.append([color[-1], img_coor, field_coor])
```

Функция преобразования для одной клетки


```python
def change_coord_sys(obj, cell_shape):
    x = obj[0]//cell_shape[0]
    y = obj[1]//cell_shape[1]
    return (x,y)

```

### Результат

После применения главной функции detect, будет получен следующий результат:


![](img/foo.png)

### Примечание

На данный момент для того, чтобы вырезать сетку нужно находить все 4 маркера. Однако на чати фотографий всего 3 маркера из 4х. В такой ситуации можно восстановить сетку по следующему алгоритму:

#### Алгоритм:
1) Находим диагональные маркеры (к примеру, верхний правый и нижний левый).

2) Строим прямую по нижним координатам верхнего маркера и внутренним правым нижнего. Пересечение двух прямых это верхний угол сетки (См. рис1).

3) Соответственные действия производим для нахождения нижнего угла сетки (См. рис2) .

4) По полученным углам восстанавливаем сетку.




![](img/find_angle1.png)
![](img/find_angle2.png)



```python

```
